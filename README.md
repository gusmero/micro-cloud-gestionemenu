# Progetto Microservizi

# Membri

- Francesco Faccioli (830190)
- Gusmara Andrea (831141)


# Introduzione

Quosto progetto si pone l'obbiettivo di creare un sistema con caratteristiche cloud-native utilizzabile per la creazione e la sua gestione d'informazione publicitaria , e di possibili servizi erogabili per un pubblico di possibili clienti di una azienda di ristorazione.



# Cloud-native application
La nostra applicazione presenterà le caratteristiche cloud-native.
Quindi utilizzeremo il processo di sviluppo DevOps , specificando pipelines e stages che permettano di ottenere l'obbiettivo di una continua Integrazione e una distribuzione continua [CI/CD](#cicd). Inoltre il nostro sistema completo presenterà un'architectura a microservizi , elencati in questo paragrafo [Microservices](#Microservices), i quali verrano poi containerizzati tramite [Docker](https://www.docker.com/) per la loro distribuzione in fase di deploying.



<a name="Microservices"></a> 
# Microservices Architecture

L'architettura del sistema presenta i seguenti microservizi:

- [Micro-cloud-HomePage](https://gitlab.com/gusmero/micro-cloud-hompage)
- [Micro-cloud-GestioneM](https://gitlab.com/gusmero/micro-cloud-gestionemenu)
- [Micro-cloud-nodejs](https://gitlab.com/ffaccioli/nodejsapi)
- [Micro-cloud-Account](https://gitlab.com/gusmero/micro-cloud-gestioneaccount)
- [Micro-cloud-APIGateway](https://gitlab.com/gusmero/micro-cloud-apigateway)



#L'applicativo 
L'applicativo è scritto in java con l'utilizzo del framework Spring . L'archittetura segue il pattern MVC  e quello a layer. La suddivisione del package è : 

- config
- controller
- service
- domain 
- repository

Questo microservizio permette di creare e cancellare oggetti di tipo menu, inoltre espone un API getMenus che restisce tutti i menu salavati in memoria.




<a name=" DevOps cicd"></a> 
# CI/CD Pipelines
Di seguito si mostrano gli stage/job scelti per la nostra pipeline(CI/CD):

- **Build**
- **Package**
- **Release**
- **Deploy**



# Build

La build del progetto viene realizzata sfruttando l'apposito goal **compile** di Maven:

```maven
mvn clean compile
```




# Package

Nello stage di package tutte i file/librerie/dipendenze utilizzate dall'applicazione vengono compresse in un unico file con estensione .jar per facilitarne la distribuzione, in quanto un unico file "compresso" richiede un minore sforzo per essere trasferito sulla rete ed elaborato da un calcolatore.
La compressione non altera la struttura dei file stessi e inoltre slega l'applicazione dalla piattaforma nativa a patto di sfruttare una JVM per la sua esecuzione.
In maven questo viene realizzato attraverso l'apposito goal **package**:

```maven
mvn clean package
```
Viene quindi prodotto un unico file .jar all'interno della cartella target pronto per essere rilasciato e distribuito.

```maven
paths:
    - target/*.jar
```


<a name="Containerized"></a>
# Release

Durante lo stage di release ci affidiamo a Docker, in particolare a [Docker Engine](https://docs.docker.com/engine/) per la creazione automatica di un'immagine (micro-cloud-homepage) a partire dalle istruzioni contenute nel Dockerfile , l'immagine viene poi pushata nel repository wyst54rhrb in DockerHub registry.
All'inetrno del Dockerfile viene definito come ENTRYPOINT il comando 
```java
java -jar "GestioneMenu-0.0.1-SNAPSHOT.jar"
```
per eseguire l'applicativo all'avvio del conteiner, tramite .jar che viene ottenuto nello stage di package , e il comando EXPOSE per esporre la porta 2222 all'inetrno del container.
Grazie ai servizi di virtualizzazione a livello di sistema operativo offerti da Docker (Platform as a Service) siamo in grado di avere un container per la nostra applicazione che potrà essere runnato su qualsiasi server.

Connessione al Docker Hub Registry:

```docker
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build -t $IMAGE_NAME .
docker push $IMAGE_NAME
```



# Deploy

Infine, per quest'ultima fase sfruttiamo il servizio di cloud platforming (PaaS) [Azure](https://portal.azure.com/), in cui è installato il sistema open-source [Kubernetes](https://kubernetes.io/) per l'automazione del deployment , scaling e il management di un applicazione contenerizzata.
Con questi due strumenti otteniamo così una configurazione di continuos deployment che risulta pronta a ricevere il deploy tramite processo DevOps.

Lo stage di deploy viene organizzato con una prima connessione alla macchina remota tramite connessione ssh; abbiamo impostato la nostra chiave pubblica RSA sul server e privata sul repository. Disabilitiamo lo StrictHostKeyChecking per fare in modo che venga accettato anche il nostro host. e settiamo come autenticazione preferita la chiave ssh PreferredAuthentications=publicKey

Dopo aver effettuato la connessione alla macchina remota, ci spostiamo nela cartella specifica e pulliamo il progetto dal repository, nel quale è presente il file YAML di configurazione per il servizio e di deployment necessari. Grazie al nostro processo cicd e le configurazione delle policy di update ,  settante a rolling update ,

```yaml
strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
```

 in ogni deployment object abbiamo così un aggiornamento da un sistema in esecuzione ad un'altro sistema in esecuzione. Permettendo una percentuale di disponibilità alta.
Infine terminiamo la connessione con la macchina remota.

```yaml
deploy:
  stage: deploy
  environment:
    name: staging
  before_script:
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
    # Esegue ssh-agent
  - eval $(ssh-agent -s)
    # Aggiunta della chiave privata
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    # Creazione della cartella SSH e impostazione dei permessi
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
    # Viene disabilitato l'host key checking
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh -o StrictHostKeyChecking=no -o PreferredAuthentications=publickey -p 61414 studente@ml-lab-a08e3194-10fa-4b3b-b775-9e0c8a18d169.westeurope.cloudapp.azure.com "cd cloud-computing-lab/progetto/menu && git checkout main && git pull origin main && kubectl apply -f pvc-menu.yaml && kubectl apply -f mysql-menu.yaml && kubectl apply -f gestione-menu.yaml && exit"
  only:
    - main
```  
