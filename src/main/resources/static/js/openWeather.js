/*
When to use JavaScript const?
As a general rule, always declare a variable with const unless you know that the value will change.
Use const when you declare:
A new Array , A new Object , A new Function ,A new RegExp

Constant objects :
// You can create a const object:
const car = {type:"Fiat", model:"500", color:"white"};
// You can change a property:
car.color = "red";
and you can't reasign new object

http://api.openweathermap.org/data/2.5/onecall?lat=45.51686960789838&lon=8.750322514022459&appid=fe30841c403c07c4ef1586c675af0548&units=metric&lang=en

*/
const app = {
  fetchWeather: () => {
    //use the values from latitude and longitude to fetch the weather
    let lat = 45.51686960789838;
    let lon = 8.750322514022459;
    let key = 'fe30841c403c07c4ef1586c675af0548';
    let lang = 'en';
    let units = 'metric';
    let url = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=${key}&units=${units}&lang=${lang}`;
    //fetch the weather
    fetch(url)
      .then((resp) => {
        if (!resp.ok) throw new Error(resp.statusText);
        return resp.json();
      })
      .then((data) => {
        app.showWeather(data);
      })
      .catch(console.err);
  },
  showWeather: (resp) => {
    console.log(resp);
    let row = document.querySelector('.weather.row');
    //clear out the old weather and add the new
    // row.innerHTML = '';
    row.innerHTML = resp.daily
      .map((day, idx) => {
        if (idx <= 2) {
          let dt = new Date(day.dt * 1000); //timestamp * 1000
          let sr = new Date(day.sunrise * 1000).toTimeString();
          let ss = new Date(day.sunset * 1000).toTimeString();
          return `<div class="col">
              <div class="card">
              <h5 class="card-title p-2">${dt.toDateString()}</h5>
                <img
                  src="https://openweathermap.org/img/wn/${
                    day.weather[0].icon
                  }@4x.png"
                  class="card-img-top"
                  alt="${day.weather[0].description}"
                />
                <div class="card-body">
                  <h3 class="card-title" style="color: black">${day.weather[0].main}</h3>
                  <p class="card-text" style="color: black">Massime ${day.temp.max}&deg;C Minime ${
            day.temp.min
          }&deg;C</p>
                  <p class="card-text" style="color: black">Massime percepite ${
                    day.feels_like.day
                  }&deg;C</p>
                  <p class="card-text" style="color: black">Pressione ${day.pressure}mb</p>
                  <p class="card-text" style="color: black">Umidità ${day.humidity}%</p>
                  <p class="card-text" style="color: black">Indice UV ${day.uvi}</p>
                  <p class="card-text" style="color: black">Precipitazioni ${day.pop * 100}%</p>
                  <p class="card-text" style="color: black">Vento ${day.wind_speed}m/s, ${
            day.wind_deg
          }&deg;</p>
                </div>
              </div>
            </div>
          </div>`;
        }
      })
      .join(' ');
  },
};

app.fetchWeather();
