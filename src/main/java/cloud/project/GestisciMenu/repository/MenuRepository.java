package cloud.project.GestisciMenu.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import cloud.project.GestisciMenu.domain.Menu;

public interface MenuRepository extends JpaRepository<Menu, String>{
	

}