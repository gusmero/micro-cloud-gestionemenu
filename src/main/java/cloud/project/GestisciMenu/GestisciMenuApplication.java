package cloud.project.GestisciMenu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
//@EnableDiscoveryClient
public class GestisciMenuApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestisciMenuApplication.class, args);
	}

}
