package cloud.project.GestisciMenu.controller;

import java.io.*;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.fasterxml.jackson.databind.ObjectMapper;  


import cloud.project.GestisciMenu.service.MenuService;

@Controller
public class MenuController {

    @Autowired
    MenuService menuService;

    ObjectMapper objectMapper=new ObjectMapper();

    @GetMapping("/index")
    public String getManageMenu(Model model){
      model.addAttribute("menus", menuService.getMenus());
      return "manageMenu";
    }


  
    
    @PostMapping("/upload")
    public  String uploadFile(@RequestParam("titolo") String titolo,
    @RequestParam("primi") String primiPiatti,
    @RequestParam("secondi") String secondiPiatti ,Model model,RedirectAttributes redirectAttributes) {
         List<String> primi= Arrays.asList(primiPiatti.split(","));
         List<String> secondi= Arrays.asList(secondiPiatti.split(","));
         try {
          menuService.store(titolo, primi , secondi);
        } catch (IOException e) {
          e.printStackTrace();
        }
         return "redirect:/gestionemenu/index";
    }


    @RequestMapping("/deleteAll")
    public String deletAll(Model model) {
      menuService.removeAll();
      return "redirect:/gestionemenu/index";
    }

    @RequestMapping("/deleteMenu/{id}")
	  public String deleteMenu(@PathVariable("id") String id) {
		  menuService.deleteMenu(id);
		  return "redirect:/gestionemenu/index";
	  }


    @GetMapping("/api/getMenus")
    public ResponseEntity<?> getAPIMenu(){
      String json="";
      try{

          json = objectMapper.writeValueAsString(menuService.getMenus());

      } catch (Exception ex) {
          ex.printStackTrace();
      }
      System.out.println(json);
      return new ResponseEntity<>(json, HttpStatus.OK);
    }

}
