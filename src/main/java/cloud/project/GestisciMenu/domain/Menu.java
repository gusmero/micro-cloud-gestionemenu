package cloud.project.GestisciMenu.domain;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "menu")
public class Menu {
	
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  private String id;

  private String titolo;

  @ElementCollection
  private List<String> primi;

  @ElementCollection
  private List<String> secondi;

  public Menu(String titolo , List<String> primi , List<String> secondi){
    this.titolo=titolo;
    this.primi=primi;
    this.secondi=secondi;
  }

  public Menu(){}

  public String getId(){
    return id;
  }

  public String getTitolo(){
    return titolo;
  }

  public void setTitolo(String titolo){
    this.titolo=titolo;
  }

  public List<String> getPrimi(){
    return this.primi;
  }

  public void setPrimi(List<String> primi){
    this.primi=primi;
  }

  public List<String> getSecondi(){
    return this.secondi;
  }

  public void setSecondi(List<String> secondi){
    this.secondi=secondi;
  }

}