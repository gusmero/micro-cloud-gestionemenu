package cloud.project.GestisciMenu.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cloud.project.GestisciMenu.domain.Menu;
import cloud.project.GestisciMenu.repository.MenuRepository;

@Service
public class MenuService {

	@Autowired
	private  MenuRepository MenuRepository;

	public Menu store(String titolo,List<String>primi ,List<String>secondi) throws IOException {
		Menu menu = new Menu(titolo, primi , secondi);
		return MenuRepository.save(menu);
	}

	public Menu getFile(String id) {
		return MenuRepository.findById(id).get();
	}

	public List<Menu> getMenus() {
		List<Menu> menus =new ArrayList<>();
		for (Menu menu: MenuRepository.findAll()) {
			menus.add(menu);
		}
		return menus;
	}


	public void removeAll() {
		MenuRepository.deleteAll();
	}

	public void deleteMenu(String id) {
		MenuRepository.deleteById(id);
	}
	
	

}