########## Dockerfile #######################
##### utilziamo la jdk-11 come usata sulla nostra applicazione spring #######
FROM openjdk:11-jre

COPY  /target/GestisciMenu-0.0.1-SNAPSHOT.jar ./


EXPOSE 5555
ENTRYPOINT ["java", "-jar", "GestisciMenu-0.0.1-SNAPSHOT.jar"]
######################################################################

